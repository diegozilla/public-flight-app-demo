//
//  FlightServiceTest.swift
//  FlightAppServicesTests
//
//  Created by Diego Espinoza on 2/01/23.
//

import XCTest
@testable import FlightAppServices

final class FlightServiceTest: XCTestCase {

    var service: FlightServiceType!
    var bundle: Bundle!
    
    override func setUpWithError() throws {
        service = FlightService(provider: FlightQueryProviderMock())
        bundle = Bundle(for: FlightServiceTest.self)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFlightInformationParsing() {
        guard let url = bundle.url(forResource: "FlightsMock", withExtension: "json"),
              let data = try? Data(contentsOf: url) else {
            assertionFailure("No mock file")
            return
        }
        
        guard let response = try? JSONDecoder.main.decode(ResponseContainer<FlightInformationResponse>.self, from: data),
              let first = response.data.first else {
            assertionFailure("Could not parse the json")
            return
        }

        let information = FlightInformation(response: first, currency: "EUR")
        XCTAssertNotNil(information)
    }
    
    func testFlightQueryResponse() async throws {
        let response = try await service.fetchRecommendedFlights(from: "prague_cz",
                                                                 adults: 1,
                                                                 children: 0,
                                                                 infants: 0,
                                                                 from: Date(),
                                                                 to: Date().nextDay,
                                                                 limit: 5)
        assert(!response.isEmpty)
    }
    
    func testFlightQueryAndCacheResponse() async throws {
        let response = try await service.fetchAndCacheRecommendedFlights(from: "prague_cz",
                                                                         adults: 1,
                                                                         children: 0,
                                                                         infants: 0,
                                                                         from: Date(),
                                                                         to: Date().nextDay)
        assert(!response.isEmpty)
    }
}

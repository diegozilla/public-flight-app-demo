//
//  FlightServiceMock.swift
//  FlightAppServicesTests
//
//  Created by Diego Espinoza on 2/01/23.
//

import Foundation
@testable import FlightAppServices

class FlightQueryProviderMock: FlightQueryProviderType {
    func getFlights(city: String, adults: Int, children: Int, infants: Int, departAfter: String, departBefore: String, limit: Int) async throws -> [FlightInformation] {
        let bundle = Bundle(for: FlightServiceTest.self)
        
        guard let url = bundle.url(forResource: "FlightsMock", withExtension: "json"),
              let data = try? Data(contentsOf: url) else {
            return []
        }

        guard let response = try? JSONDecoder.main.decode(ResponseContainer<FlightInformationResponse>.self, from: data) else {
            return []
        }
        
        let information = response.data.map {
            FlightInformation.init(response: $0, currency: "EUR")
        }
        
        return information
    }
}

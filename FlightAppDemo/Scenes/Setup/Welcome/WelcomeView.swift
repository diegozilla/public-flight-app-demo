//
//  WelcomeView.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 28/12/22.
//

import SwiftUI
import FlightAppComponents

struct WelcomeView: View {
    @Environment(\.setupRouter) var setupRouter
    @State private var screenHeight: CGFloat = .zero
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: .large) {
                Text(L.welcomeTitle)
                    .fixedSize(horizontal: false, vertical: true)
                    .largeTitleStyle()
                    .foregroundColor(.label)
                
                Text(L.welcomeSubtitle)
                    .subtitleStyle()
                    .foregroundColor(.secondaryLabel)
                
                SolidButton(title: L.welcomeAction,
                            useMaxWidth: false) {
                    setupRouter.showCities()
                }
            }
            .padding(.top, screenHeight * 0.5)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.horizontal, .medium)
        }
        .readSize { size in
            screenHeight = size.height
        }
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}

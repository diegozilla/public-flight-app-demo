//
//  +CityRow.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 28/12/22.
//

import SwiftUI

extension CitySelectionView {
    struct CityRow: View {
        let name: String
        let isSelected: Bool
        let onAction: () -> Void

        var body: some View {
            Button {
                onAction()
            } label: {
                HStack {
                    Text(name)
                        .foregroundColor(.label)
                        .font(.body)
                        .frame(maxWidth: .infinity, alignment: .leading)

                    if isSelected {
                        Image(systemName: "checkmark.circle.fill")
                    }
                }
            }
        }
    }
}

struct CitySelectionView_CityRow_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            CitySelectionView.CityRow(name: "Prague",
                                      isSelected: true,
                                      onAction: {})
            .padding()

            CitySelectionView.CityRow(name: "London",
                                      isSelected: true,
                                      onAction: {})
            .padding()
        }
        .padding()
    }
}

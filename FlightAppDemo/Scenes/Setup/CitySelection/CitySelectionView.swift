//
//  CitySelectionView.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 28/12/22.
//

import SwiftUI
import FlightAppComponents
import FlightAppServices

struct CityViewData: Decodable, Identifiable, Hashable {
    let name: String
    let value: String
    var id: String { value }
}

struct CitySelectionView: View {
    @StateObject var viewModel: CitySelectionViewModel
    @Environment(\.setupRouter) var setupRouter
    @Environment(\.flightRouter) var flightRouter
    
    var body: some View {
        VStack(alignment: .leading) {
            List {
                Section {
                    ForEach(viewModel.availableCities) { city in
                        CityRow(name: city.name,
                                isSelected: city == viewModel.selectedCity,
                                onAction: {
                            viewModel.selectedCity = city
                        })
                        .padding(.vertical, .small)
                    }
                } footer: {
                    Text(L.citySelectionFootnote)
                        .detailTextStyle()
                        .foregroundColor(.secondaryLabel)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .listRowBackground(Color.clear)
                        .padding(.top, .small)
                }
            }
            
            SolidButton(title: viewModel.actionTitle,
                        onAction: {
                viewModel.saveSelectedCity()
                if viewModel.isModal {
                    flightRouter.dismissModal()
                } else {
                    setupRouter.showPeople()
                }
            })
            .padding()
            .disabled(viewModel.selectedCity == nil)
        }
        .task {
            viewModel.fetchCities()
        }
        .navigationBarTitleDisplayMode(.large)
        .navigationTitle(L.citySelectionTitle)
        .background(Color(uiColor: .systemGroupedBackground))
    }
}

struct CitySelectionView_Previews: PreviewProvider {
    static let factory = ViewModelFactory()
    
    static var previews: some View {
        NavigationStack {
            CitySelectionView(viewModel: factory.makeCitySelectionViewModel(isModal: false))
        }
    }
}

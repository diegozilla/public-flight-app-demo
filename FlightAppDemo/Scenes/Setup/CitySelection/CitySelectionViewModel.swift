//
//  CitySelectionViewModel.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 28/12/22.
//

import Foundation
import Combine
import FlightAppServices

final class CitySelectionViewModel: ObservableObject {
    @Published var availableCities: [CityViewData] = []
    @Published var selectedCity: CityViewData?

    @Published var isLoading = false
    @Published var errorMessage: String?
    
    var isModal: Bool
    var actionTitle: String {
        isModal ? "Save" : "Continue"
    }
    
    private let cityService: CityServiceType
    private let setupService: SetupServiceType
    private let setupStore: SetupStoreType
    
    init(cityService: CityServiceType,
         setupService: SetupServiceType,
         setupStore: SetupStoreType,
         isModal: Bool) {
        self.setupStore = setupStore
        self.cityService = cityService
        self.setupService = setupService
        self.isModal = isModal
    }

    @MainActor func fetchCities() {
        Task(priority: .high) {
            do {
                isLoading = true
                let cities = try await cityService.fetchAvailableCities()
                self.availableCities = cities.map { CityViewData(name: $0.name, value: $0.code) }
                self.selectedCity = try await getSelectedCity(from: cities)
                isLoading = false
            } catch {
                isLoading = false
                errorMessage = error.localizedDescription
            }
        }
    }
    
    func saveSelectedCity() {
        Task {
            guard let city = selectedCity?.value else { return }
            await setupStore.save(currentCity: city)
        }
    }
    
    private func getSelectedCity(from availableCities: [AvailableCity]) async throws -> CityViewData? {
        let cityCode = try await setupService.getCurrentCity()
        guard let city = availableCities.first(where: { $0.code == cityCode }) else { return nil }
        return CityViewData(name: city.name, value: city.code)
    }
}

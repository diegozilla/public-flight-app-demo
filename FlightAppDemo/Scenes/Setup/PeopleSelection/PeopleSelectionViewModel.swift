//
//  PeopleSelectionViewModel.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 28/12/22.
//

import Foundation
import Combine
import FlightAppServices

final class PeopleSelectionViewModel: ObservableObject {
    @Published var numberOfAdults = 1
    @Published var numberOfChildren = 0
    @Published var numberOfInfants = 0
    @Published var canFinish = false

    var canAddPeople: Bool {
        let numberOfPeople = numberOfAdults + numberOfChildren + numberOfInfants
        return numberOfPeople < 9
    }
    var actionTitle: String {
        isModal ? "Save" : "Finish"
    }
    
    var isModal: Bool

    private let setupService: SetupServiceType
    private let setupStore: SetupStoreType
    private var cancellables = Set<AnyCancellable>()
    
    init(setupService: SetupServiceType,
         setupStore: SetupStoreType,
         isModal: Bool) {
        self.setupStore = setupStore
        self.isModal = isModal
        self.setupService = setupService
        setupBindings()
    }
    
    @MainActor func fetchPassengers() async {
        let passengers = try? await self.setupService.getPassengers()
        guard let passengers = passengers else { return }
        self.numberOfAdults = passengers.adults
        self.numberOfInfants = passengers.infants
        self.numberOfChildren = passengers.children
    }
    
    private func setupBindings() {
        Publishers.CombineLatest3($numberOfAdults, $numberOfChildren, $numberOfInfants)
            .map { adults, children, infants in
                adults + children + infants >= 1 && adults >= 1
            }
            .assign(to: &$canFinish)
    }
    
    func addAdults() {
        guard canAddPeople else { return }
        numberOfAdults += 1
    }
    
    func removeAdult() {
        guard numberOfAdults > 0 else { return }
        numberOfAdults -= 1
    }
    
    func addChildren() {
        guard canAddPeople else { return }
        numberOfChildren += 1
    }
    
    func removeChildren() {
        guard numberOfChildren > 0 else { return }
        numberOfChildren -= 1
    }
    
    func addInfants() {
        guard canAddPeople else { return }
        numberOfInfants += 1
    }
    
    func removeInfants() {
        guard numberOfInfants > 0 else { return }
        numberOfInfants -= 1
    }
    
    func savePassengers() {
        Task {
            await setupStore.save(adults: numberOfAdults,
                                  children: numberOfChildren,
                                  infants: numberOfInfants)
        }
    }
}

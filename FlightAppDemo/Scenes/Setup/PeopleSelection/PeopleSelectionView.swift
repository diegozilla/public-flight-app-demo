//
//  PeopleSelectionView.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 28/12/22.
//

import SwiftUI
import FlightAppComponents

struct PeopleSelectionView: View {
    @StateObject var viewModel: PeopleSelectionViewModel
    @Environment(\.appRouter) var appRouter
    @Environment(\.flightRouter) var flightRouter

    var body: some View {
        VStack {
            ScrollView {
                VStack {
                    Text(L.peopleInstructions)
                        .foregroundColor(.secondaryLabel)
                        .detailTextStyle()
                        .padding(.vertical, .small)
                        .frame(maxWidth: .infinity, alignment: .leading)

                    PeopleStepperView(title: L.peopleAdults,
                                      number: viewModel.numberOfAdults,
                                      onIncrement: { viewModel.addAdults() },
                                      onDecrement: { viewModel.removeAdult() })
                    
                    PeopleStepperView(title: L.peopleChildren,
                                      number: viewModel.numberOfChildren,
                                      onIncrement: { viewModel.addChildren() },
                                      onDecrement: { viewModel.removeChildren() })
                    
                    PeopleStepperView(title: L.peopleInfants,
                                      number: viewModel.numberOfInfants,
                                      onIncrement: { viewModel.addInfants() },
                                      onDecrement: { viewModel.removeInfants() })
                }
                .padding(.horizontal, .medium)
                .navigationBarTitleDisplayMode(.large)
                .navigationTitle(L.peopleTitle)
            }
            
            SolidButton(title: viewModel.actionTitle) {
                viewModel.savePassengers()
                if viewModel.isModal {
                    flightRouter.dismissModal()
                } else {
                    withAnimation { appRouter.didFinishSetup(true) }
                }
            }
            .padding()
            .disabled(!viewModel.canFinish)
        }
        .task {
            await viewModel.fetchPassengers()
        }
    }
}

struct PeopleSelectionView_Previews: PreviewProvider {
    static let factory = ViewModelFactory()

    static var previews: some View {
        NavigationStack {
            PeopleSelectionView(viewModel: factory.makePeopleSelectionViewModel(isModal: false))
        }
    }
}

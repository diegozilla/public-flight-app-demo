//
//  +PeopleStepperView.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 29/12/22.
//

import SwiftUI

extension PeopleSelectionView {
    struct PeopleStepperView: View {
        let title: String
        let number: Int
        var onIncrement: () -> Void
        var onDecrement: () -> Void
        
        var body: some View {
            HStack {
                Text(title)
                    .informationTextStyle()
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                Button("-") {
                    onDecrement()
                }
                .buttonStyle(.bordered)
                
                Text("\(number)")
                    .font(.callout)
                    .fontWeight(.bold)
                    .padding(.horizontal, .small)
                    .frame(width: 55)
                
                Button("+") {
                    onIncrement()
                }
                .buttonStyle(.bordered)
            }
            .padding(.vertical, .small)
            .padding(.horizontal, .small)
        }
    }
}

struct PeopleSelectionView_PeopleStepperView_Previews: PreviewProvider {
    static let factory = ViewModelFactory()

    static var previews: some View {
        VStack {
            PeopleSelectionView.PeopleStepperView(title: "Adults",
                                                  number: 1,
                                                  onIncrement: {},
                                                  onDecrement: {})
            
            PeopleSelectionView.PeopleStepperView(title: "Children",
                                                  number: 1,
                                                  onIncrement: {},
                                                  onDecrement: {})
        }
    }
}


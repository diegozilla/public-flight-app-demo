//
//  FlightDetailView.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 30/12/22.
//

import SwiftUI
import FlightAppComponents
import FlightAppServices

struct FlightDetailView: View {
    @StateObject var viewModel: FlightDetailViewModel
    
    var body: some View {
        ScrollView(showsIndicators: false) {
            LazyVStack(alignment: .leading) {
                CityImageView(
                    url: viewModel.flightDetailViewData.imageURL,
                    opacity: 0.35
                )
                .cornerRadius(.medium)

                Group {
                    Text(viewModel.flightDetailViewData.city)
                        .largeTitleStyle()
                    
                    Text(viewModel.flightDetailViewData.country)
                        .secondaryTitleStyle()
                        .foregroundColor(.secondaryLabel)
                    
                    FlightDetailSectionView(sectionTitle: L.flightDetailGeneralInformation,
                                            content: generalInformation)
                    .padding(.top, .large)
                    
                    FlightDetailSectionView(sectionTitle: L.flightDetailTravelRoute,
                                            content: travelRoute)
                    .padding(.top, .large)
                    
                    FlightDetailSectionView(sectionTitle: L.flightDetailMore,
                                            content: moreInformation)
                    .padding(.vertical, .large)
                }
                .padding(.horizontal, .small)
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.horizontal, .small)
            .padding(.top, .small)
        }
        .background(Color.groupedBackground)
        .navigationBarTitleDisplayMode(.inline)
    }

    private var generalInformation: some View {
        VStack(spacing: .medium) {
            FlightDetailRow(title: L.flightDetailDepartureTime,
                            detail: viewModel.flightDetailViewData.departureTime)
            
            Divider()
            
            FlightDetailRow(title: L.flightDetailArrivalTime,
                            detail: viewModel.flightDetailViewData.arrivalTime)
            
            Divider()
            
            FlightDetailRow(title: L.flightDetailDuration,
                            detail: viewModel.flightDetailViewData.duration)
            
            Divider()
            
            FlightDetailRow(title: L.flightDetailPrice,
                            detail: viewModel.flightDetailViewData.price)
        }
    }
    
    private var travelRoute: some View {
        VStack(spacing: .large) {
            ForEach(viewModel.flightDetailViewData.routes) { route in
                FlightDetailRouteCard(route: route)
                
                if route != viewModel.flightDetailViewData.routes.last {
                    Divider()
                }
            }
        }
    }
    
    private var moreInformation: some View {
        Button(L.flightDetailGoToWeb) {
            viewModel.openWebsite()
        }
        .font(.body)
        .fontWeight(.semibold)
    }
}

struct FlightDetailView_Previews: PreviewProvider {
    static let factory = ViewModelFactory()
    
    static var previews: some View {
        NavigationStack {
            FlightDetailView(viewModel: factory.makeFlightDetailViewModel(flightInformation: .stub))
        }
    }
}

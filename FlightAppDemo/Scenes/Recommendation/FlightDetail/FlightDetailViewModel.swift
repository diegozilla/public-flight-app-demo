//
//  FlightDetailViewModel.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 30/12/22.
//

import UIKit
import Foundation
import Combine
import FlightAppServices

final class FlightDetailViewModel: ObservableObject {
    var flightDetailViewData: FlightDetailViewData
    private let deeplinkURL: URL?

    init(flightInformation: FlightInformation) {
        self.flightDetailViewData = FlightDetailViewData(flightInformation: flightInformation)
        self.deeplinkURL = flightInformation.deeplinkURL
    }
    
    func openWebsite() {
        guard let deeplinkURL = deeplinkURL else { return }
        UIApplication.shared.open(deeplinkURL)
    }
}

struct FlightDetailViewData {
    let id: String
    let imageURL: URL?
    let city: String
    let country: String
    
    let price: String
    let duration: String
    
    let departureTime: String
    let arrivalTime: String
    
    let routes: [Route]
    
    init(flightInformation: FlightInformation) {
        let formattedPrice = NumberFormatter.currencyFormatter.string(from: NSNumber(value: flightInformation.price))
        self.id = flightInformation.id
        self.imageURL = flightInformation.imageURL
        self.city = flightInformation.destination
        self.country = flightInformation.destinationCountry
        self.price = "\(formattedPrice ?? "N/A") \(flightInformation.currency)"
        self.duration = flightInformation.flyDuration
        self.departureTime = flightInformation.departureTime.completeFlightFormat
        self.arrivalTime = flightInformation.arrivalTime.completeFlightFormat
        self.routes = flightInformation.route.map(Route.init)
    }
    
    struct Route: Identifiable, Equatable {
        let id: String

        let departureName: String
        let arrivalName: String
        
        let departureDay: String
        let arrivalDay: String
        
        let departureHour: String
        let arrivalHour: String
        
        let flightInformation: String
        let flightNumber: Int
        
        var formattedFlightNumber: String {
            NumberFormatter.flightFormatter.string(from: NSNumber(value: flightNumber)) ?? "N/A"
        }
        
        init(route: FlightInformation.Route) {
            self.id = route.id
            self.flightInformation = route.airline
            self.flightNumber = route.flightNumber
            self.departureName = route.cityFrom
            self.arrivalName = route.cityTo
            self.departureDay = DateFormatter.flightDateFormatter.string(from: route.departureDate)
            self.arrivalDay = DateFormatter.flightDateFormatter.string(from: route.arrivalDate)
            self.departureHour = DateFormatter.flightHourFormatter.string(from: route.departureDate)
            self.arrivalHour = DateFormatter.flightHourFormatter.string(from: route.arrivalDate)
        }
    }
}

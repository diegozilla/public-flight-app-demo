//
//  +FlightDetailSectionView.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 30/12/22.
//

import SwiftUI
import FlightAppServices

extension FlightDetailView {
    struct FlightDetailSectionView<Content: View>: View {
        let sectionTitle: String
        let content: Content
        
        var body: some View {
            VStack(alignment: .leading, spacing: .tiny) {
                Text(sectionTitle)
                    .foregroundColor(.label)
                    .sectionTitleStyle()
                    .padding(.bottom, .small)
                
                VStack(spacing: .large) {
                    content
                }
                .padding(.vertical)
                .padding(.horizontal, .medium)
                .frame(maxWidth: .infinity, alignment: .leading)
                .background(Color.secondaryGroupedBackground)
                .cornerRadius(.small)
            }
        }
    }
}

struct FlightDetailSectionView_Previews: PreviewProvider {
    private static var row: FlightDetailView.FlightDetailRow {
        FlightDetailView.FlightDetailRow(title: "Content", detail: "Detail")
    }
    
    private static var flightStub = FlightDetailViewData(flightInformation: .stub)
    
    private static var route: some View {
        VStack(spacing: .large) {
            FlightDetailView.FlightDetailRouteCard(route: flightStub.routes[0])
            
            Divider()
            
            FlightDetailView.FlightDetailRouteCard(route: flightStub.routes[0])
        }
    }
    
    static var previews: some View {
        VStack {
            FlightDetailView.FlightDetailSectionView(sectionTitle: "Hello",
                                                     content: row)
                .padding()
            
            FlightDetailView.FlightDetailSectionView(sectionTitle: "Hello",
                                                     content: route)
                .padding()
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.groupedBackground)
    }
}

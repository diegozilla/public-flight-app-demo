//
//  +FlightDetailRow.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 30/12/22.
//

import SwiftUI

extension FlightDetailView {
    struct FlightDetailRow: View {
        let title: String
        let detail: String
        
        var body: some View {
            HStack {
                Text(title)
                    .foregroundColor(.label)
                    .informationTextStyle()
                
                Spacer()
                
                Text(detail)
                    .foregroundColor(.secondaryLabel)
                    .detailTextStyle()
            }
        }
    }
}

struct FlightDetailView_FlightDetailRow_Previews: PreviewProvider {
    static let factory = ViewModelFactory()
    
    static var previews: some View {
        VStack {
            FlightDetailView.FlightDetailRow(title: "Detail", detail: "Information")
                .padding()
            FlightDetailView.FlightDetailRow(title: "Detail", detail: "Information")
                .padding()
            FlightDetailView.FlightDetailRow(title: "Detail", detail: "Information")
                .padding()
        }
    }
}

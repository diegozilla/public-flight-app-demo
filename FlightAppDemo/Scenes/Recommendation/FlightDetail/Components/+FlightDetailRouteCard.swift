//
//  +FlightDetailRouteCard.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 30/12/22.
//

import SwiftUI
import FlightAppServices

extension FlightDetailView {
    struct FlightDetailRouteCard: View {
        let route: FlightDetailViewData.Route
        
        var body: some View {
            VStack(alignment: .leading) {
                Text("\(L.flightDetailFlightTitle) \(route.flightInformation)-\(route.formattedFlightNumber)")
                    .smallTextStyle()
                    .foregroundColor(.secondaryLabel)
                
                HStack {
                    item(forDestination: route.departureName,
                         tintColor: .green,
                         day: route.departureDay,
                         hour: route.departureHour,
                         icon: "airplane.departure")
                    
                    Spacer()
                    
                    Image(systemName: "arrow.forward")
                        .foregroundColor(.secondaryLabel)
                    
                    Spacer()
                    
                    item(forDestination: route.arrivalName,
                         tintColor: .orange,
                         day: route.arrivalDay,
                         hour: route.arrivalHour,
                         icon: "airplane.arrival")
                }
                .padding(.top, .small)
            }
        }
        
        private func item(forDestination destination: String,
                          tintColor: Color,
                          day: String,
                          hour: String,
                          icon: String) -> some View {
            VStack(spacing: .tiny) {
                Image(systemName: icon)
                    .foregroundColor(tintColor)
                
                Text(destination)
                    .foregroundColor(.label)
                    .font(.headline)
                    .fontWeight(.medium)
                    .padding(.top, .small)
                
                Text(day)
                    .foregroundColor(.secondaryLabel)
                    .detailTextStyle()
                
                Text(hour)
                    .foregroundColor(.secondaryLabel)
                    .detailTextStyle()
            }
        }
    }
}

struct FlightDetailView_FlightDetailRouteCard_Previews: PreviewProvider {
    static var routes: [FlightDetailViewData.Route] {
        FlightDetailViewData(flightInformation: .stub).routes
    }
    
    static var previews: some View {
        VStack {
            FlightDetailView.FlightDetailRouteCard(route: routes[0])
                .padding()
                .background(Color.secondaryGroupedBackground)
            
            FlightDetailView.FlightDetailRouteCard(route: routes[0])
                .padding()
                .background(Color.secondaryGroupedBackground)
        }
        .padding()
        .background(Color.groupedBackground)
    }
}

//
//  +FlightEmptyView.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 30/12/22.
//

import SwiftUI
import FlightAppComponents

extension FlightsView {
    struct FlightEmptyView: View {
        let onTryAgain: () -> Void
        
        var body: some View {
            VStack(spacing: .small) {
                Text(L.flightEmptyStateTitle)
                    .sectionTitleStyle()
                    .multilineTextAlignment(.center)
                
                SolidButton(title: L.flightEmptyStateTry,
                            useMaxWidth: false) {
                    onTryAgain()
                }
                            .padding(.top, .xLarge)
            }
        }
    }
}

struct FlightEmptyView_Previews: PreviewProvider {
    static var previews: some View {
        FlightsView.FlightEmptyView(onTryAgain: {})
        .padding(.horizontal, .large)
    }
}

//
//  +FlightItem.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 29/12/22.
//

import SwiftUI
import FlightAppComponents
import FlightAppServices

extension FlightsView {
    struct FlightItemView: View {
        let viewData: FlightItemViewData
        let onAction: (String) -> Void

        var body: some View {
            VStack(alignment: .leading) {
                CityImageView(url: viewData.imageURL)

                VStack(alignment: .leading, spacing: .zero) {
                    Text(viewData.city)
                        .secondaryBoldTitleStyle()
                        .foregroundColor(.label)
                    
                    Text(viewData.country)
                        .subtitleStyle()
                        .foregroundColor(.secondaryLabel)
                    
                    HStack(alignment: .lastTextBaseline) {
                        Text(viewData.price)
                            .smallTextStyle()
                            .foregroundColor(.label)
                            .padding(.top, .xSmall)
                        
                        Spacer()

                        HStack(spacing: .tiny) {
                            Image(systemName: "airplane.departure")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: .medium, height: .medium)
                                .foregroundColor(.secondary)
                            
                            Text(viewData.duration)
                                .font(.footnote)
                                .fontWeight(.regular)
                                .foregroundColor(.secondary)
                        }
                    }
                }
                .padding(.top, .tiny)
                .padding([.horizontal, .bottom], .medium)
            }
            .background(Color.secondaryGroupedBackground)
            .cornerRadius(.xSmall)
            .padding(.horizontal)
            .onTapGesture {
                onAction(viewData.id)
            }
        }
    }
}

struct FlightItem_Previews: PreviewProvider {
    static var previews: some View {
        ScrollView {
            VStack(spacing: .large) {
                FlightsView.FlightItemView(viewData: .init(flightInformation: .stub),
                                           onAction: { _ in })
                
                FlightsView.FlightItemView(viewData: .init(flightInformation: .badStub),
                                           onAction: { _ in })
            }
        }
        .background(Color.groupedBackground)
    }
}

//
//  +FlightCityImage.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 2/01/23.
//

import SwiftUI

struct CityImageView: View {
    let url: URL?
    var opacity: CGFloat = 0.25

    var body: some View {
        AsyncImage(url: url,
                   content: { image in
            image
                .resizable()
                .scaledToFit()
                .overlay {
                    Color.black.opacity(opacity)
                }
        }, placeholder: {
            Image("city_placeholder")
                .resizable()
                .scaledToFit()
                .overlay {
                    Color.black.opacity(opacity)
                }
        })
    }
}

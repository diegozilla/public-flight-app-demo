//
//  FlightsView.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 28/12/22.
//

import SwiftUI
import FlightAppComponents

struct FlightsView: View {
    @StateObject var viewModel: FlightsViewModel

    @Environment(\.appRouter) var appRouter
    @Environment(\.flightRouter) var flightRouter
    @Environment(\.viewModelFactory) var factory
    
    @State private var height: CGFloat = .zero

    var body: some View {
        StateContainerView(error: viewModel.errorMessage,
                           isLoading: viewModel.viewState == .loading,
                           content: content)
        .background(Color.groupedBackground)
    }
    
    private var content: some View {
        ScrollView {
            VStack(spacing: .medium) {
                switch viewModel.viewState {
                case .empty:
                    FlightEmptyView(onTryAgain: {
                        viewModel.fetchFlights()
                    })
                        .padding(.top, height * 0.4)
                        .padding(.horizontal, .large)
                case .content:
                    ForEach(viewModel.flights, id: \.id) { flight in
                        FlightItemView(viewData: flight) { _ in
                            guard let information = viewModel.getFlightInformation(for: flight.id) else { return }
                            flightRouter.showDetail(flight: information)
                        }
                    }
                    .padding(.bottom, .medium)
                case .loading:
                    EmptyView()
                }
            }
        }
        .task {
            viewModel.fetchInitialFlights()
        }
        .transaction { t in
            t.animation = nil
            t.disablesAnimations = true
        }
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button {
                    flightRouter.presentCitySetup()
                } label: {
                    Image(systemName: "mappin.circle.fill")
                }
            }
            
            ToolbarItem(placement: .navigationBarTrailing) {
                Button {
                    flightRouter.presentPeopleSetup()
                } label: {
                    Image(systemName: "person.fill.badge.plus")
                }
            }
        }
        .navigationTitle(L.flightSuggestionTitle)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .readSize { size in
            height = size.height
        }
    }
}

struct FlightsView_Previews: PreviewProvider {
    static let factory = ViewModelFactory()
    static var viewModel: FlightsViewModel {
        let vm = factory.makeFlightViewModel()
        vm.flights = [FlightItemViewData(flightInformation: .stub)]
        vm.viewState = .content
        return vm
    }

    static var previews: some View {
        FlightsView(viewModel: viewModel)
    }
}

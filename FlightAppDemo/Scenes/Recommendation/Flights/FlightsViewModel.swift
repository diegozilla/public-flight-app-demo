//
//  FlightsViewModel.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation
import Combine
import FlightAppServices

enum FlightViewState {
    case loading
    case empty
    case content
}

final class FlightsViewModel: ObservableObject {
    @Published var flights: [FlightItemViewData] = []
    @Published var viewState = FlightViewState.loading
    @Published var errorMessage: String?
    
    private let flightService: FlightServiceType
    private let setupService: SetupServiceType
    private let setupStore: SetupStoreType
    private var viewDidAppear = false
    private var searchDate = Date()
    private var flightInformation: [FlightInformation] = []
    private var cancellables = Set<AnyCancellable>()

    init(flightService: FlightServiceType,
         setupService: SetupServiceType,
         setupStore: SetupStoreType) {
        self.setupStore = setupStore
        self.flightService = flightService
        self.setupService = setupService
        setupBindings()
    }

    private func setupBindings() {
        setupStore.city
            .removeDuplicates()
            .dropFirst()
            .sink { [weak self] _ in
                self?.getFlights()
            }
            .store(in: &cancellables)

        setupStore.passengers
            .removeDuplicates()
            .dropFirst()
            .sink { [weak self] _ in
                self?.getFlights()
            }
            .store(in: &cancellables)
        
        $errorMessage
            .dropFirst(1)
            .removeDuplicates()
            .delay(for: .seconds(3), scheduler: DispatchQueue.main)
            .filter { $0 != nil }
            .map { _ in nil }
            .assign(to: &$errorMessage)
    }
    
    private func getFlights() {
        Task.detached { await self.fetchFlights() }
    }
    
    func getFlightInformation(for id: String) -> FlightInformation? {
        flightInformation.first(where: { $0.id == id })
    }
    
    @MainActor func fetchFlights() {
        Task(priority: .userInitiated) {
            do {
                viewState = .loading
                let city = try await setupService.getCurrentCity()
                let passengers = try await setupService.getPassengers()
                let flights = try await flightService.fetchAndCacheRecommendedFlights(from: city,
                                                                                      adults: passengers.adults,
                                                                                      children: passengers.children,
                                                                                      infants: passengers.infants,
                                                                                      from: Date(),
                                                                                      to: Date().nextDay)
                self.flightInformation = flights
                self.flights = flights.map(FlightItemViewData.init)
                viewState = flights.isEmpty ? .empty : .content
            } catch {
                viewState = .empty
                errorMessage = error.localizedDescription
            }
        }
    }

    func fetchInitialFlights() {
        if !viewDidAppear {
            viewDidAppear = true
            getFlights()
        }
    }
}

struct FlightItemViewData: Identifiable {
    let id: String
    let city: String
    let country: String
    let imageURL: URL?
    let price: String
    let duration: String
    
    init(flightInformation: FlightInformation) {
        let formattedPrice = NumberFormatter.currencyFormatter.string(from: NSNumber(value: flightInformation.price))
        self.id = flightInformation.id
        self.city = flightInformation.destination
        self.country = flightInformation.destinationCountry
        self.imageURL = flightInformation.imageURL
        self.price = "\(formattedPrice ?? "N/A") \(flightInformation.currency)"
        self.duration = flightInformation.flyDuration
    }
}

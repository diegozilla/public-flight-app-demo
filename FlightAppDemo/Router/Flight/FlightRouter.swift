//
//  FlightRouter.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 30/12/22.
//

import Foundation
import Combine
import SwiftUI
import FlightAppServices

private struct FlightKey: EnvironmentKey {
    static let defaultValue: FlightRouter = .init()
}

extension EnvironmentValues {
    var flightRouter: FlightRouter {
        get { self[FlightKey.self] }
        set { self[FlightKey.self] = newValue }
    }
}

enum FlightScreen: Equatable, Hashable {
    case list
    case detail(FlightInformation)
    
    private var value: String {
        switch self {
        case .list: return "list"
        case .detail(let information): return "detail \(information.id)"
        }
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(value)
    }
    
    static func == (lhs: FlightScreen, rhs: FlightScreen) -> Bool {
        switch (lhs, rhs) {
        case (.list, .list): return true
        case (.detail, .detail): return true
        default: return false
        }
    }
}

enum FlightModal: String, Identifiable {
    case city
    case people
    
    var id: String { self.rawValue }
}

final class FlightRouter: ObservableObject {
    @Published var screens: [FlightScreen] = []
    @Published var modal: FlightModal?
    
    func showList() {
        screens.append(.list)
    }
    
    func showDetail(flight: FlightInformation) {
        screens.append(.detail(flight))
    }

    func presentCitySetup() {
        modal = .city
    }
    
    func presentPeopleSetup() {
        modal = .people
    }

    func dismissModal() {
        modal = nil
    }
}

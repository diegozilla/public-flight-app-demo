//
//  FlightRouterView.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 30/12/22.
//

import SwiftUI

struct FlightRouterView: View {
    @StateObject private var flightRouter = FlightRouter()
    
    @Environment(\.viewModelFactory) var factory
    
    var body: some View {
        NavigationStack(path: $flightRouter.screens) {
            FlightsView(
                viewModel: factory.makeFlightViewModel()
            )
                .environment(\.flightRouter, flightRouter)
        }
        .sheet(item: $flightRouter.modal, content: { modal in
            NavigationStack {
                switch modal {
                case .city:
                    CitySelectionView(
                        viewModel: factory.makeCitySelectionViewModel(isModal: true)
                    )
                case .people:
                    PeopleSelectionView(
                        viewModel: factory.makePeopleSelectionViewModel(isModal: true)
                    )
                }
            }
            .environment(\.flightRouter, flightRouter)
        })
        .navigationDestination(for: FlightScreen.self) { screen in
            Group {
                switch screen {
                case .list:
                    FlightsView(
                        viewModel: factory.makeFlightViewModel()
                    )
                case .detail(let flightInformation):
                    FlightDetailView(
                        viewModel: factory.makeFlightDetailViewModel(flightInformation: flightInformation)
                    )
                }
            }
            .environment(\.flightRouter, flightRouter)
        }
    }
}

struct FlightRouterView_Previews: PreviewProvider {
    static var previews: some View {
        FlightRouterView()
    }
}

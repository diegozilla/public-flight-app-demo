//
//  AppRouter.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 28/12/22.
//

import Foundation
import Combine
import SwiftUI
import FlightAppServices

private struct AppRouterKey: EnvironmentKey {
    static let defaultValue: AppRouter = .init()
}

extension EnvironmentValues {
    var appRouter: AppRouter {
        get { self[AppRouterKey.self] }
        set { self[AppRouterKey.self] = newValue }
    }
}

final class AppRouter: ObservableObject {
    @Published var hasFinishedSetup = false

    private var cancellables = Set<AnyCancellable>()
    
    init() {
        setupRouter()
    }
    
    func didFinishSetup(_ didFinish: Bool) {
        hasFinishedSetup = didFinish
    }
    
    private func setupRouter() {
        hasFinishedSetup = UserDefaults.fetch(usingKey: .hasFinishedSetup) ?? false

        $hasFinishedSetup
            .sink { hasFinished in
                UserDefaults.save(hasFinished, usingKey: .hasFinishedSetup)
            }
            .store(in: &cancellables)
    }
}

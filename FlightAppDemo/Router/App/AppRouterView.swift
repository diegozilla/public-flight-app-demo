//
//  AppRouterView.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 28/12/22.
//

import SwiftUI

extension AnyTransition {
    static var routerTransition: AnyTransition {
        .asymmetric(insertion: .move(edge: .bottom).combined(with: .opacity),
                    removal: .move(edge: .bottom).combined(with: .opacity))
    }
}

struct AppRouterView: View {
    @StateObject var appRouter = AppRouter()
    
    private var factory: ViewModelFactory
    
    init(factory: ViewModelFactory) {
        self.factory = factory
    }

    var body: some View {
        Group {
            if appRouter.hasFinishedSetup {
                NavigationStack {
                    FlightRouterView()
                }
                .transition(.routerTransition)
            } else {
                NavigationStack {
                    SetupRouterView()
                }
                .transition(.routerTransition)
            }
        }
        .animation(.easeInOut(duration: 0.5), value: appRouter.hasFinishedSetup)
        .environment(\.appRouter, appRouter)
        .environment(\.viewModelFactory, factory)
    }
}

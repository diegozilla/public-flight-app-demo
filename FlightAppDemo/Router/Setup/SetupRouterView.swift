//
//  SetupRouterView.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 28/12/22.
//

import SwiftUI

struct SetupRouterView: View {
    @StateObject var setupRouter = SetupRouter()

    @Environment(\.viewModelFactory) var factory

    var body: some View {
        NavigationStack(path: $setupRouter.screens) {
            WelcomeView()
                .environment(\.setupRouter, setupRouter)
        }
        .navigationDestination(for: SetupScreens.self) { screen in
            Group {
                switch screen {
                case .welcome:
                    WelcomeView()
                case .cities:
                    CitySelectionView(
                        viewModel: factory.makeCitySelectionViewModel(isModal: false)
                    )
                case .people:
                    PeopleSelectionView(
                        viewModel: factory.makePeopleSelectionViewModel(isModal: false)
                    )
                }
            }
            .environment(\.setupRouter, setupRouter)
        }
    }
}

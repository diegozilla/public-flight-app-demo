//
//  SettingsRouter.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 28/12/22.
//

import Foundation
import SwiftUI

private struct SetupRouterKey: EnvironmentKey {
    static let defaultValue: SetupRouter = .init()
}

extension EnvironmentValues {
    var setupRouter: SetupRouter {
        get { self[SetupRouterKey.self] }
        set { self[SetupRouterKey.self] = newValue }
    }
}

enum SetupScreens {
    case welcome
    case cities
    case people
}

final class SetupRouter: ObservableObject {
    @Published var screens: [SetupScreens] = []
    
    func showWelcome() {
        screens.append(.welcome)
    }
    
    func showCities() {
        screens.append(.cities)
    }
    
    func showPeople() {
        screens.append(.people)
    }
}

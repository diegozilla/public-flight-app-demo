//
//  ViewModelFactory.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation
import SwiftUI
import FlightAppServices

private struct ViewModelFactoryKey: EnvironmentKey {
    static let defaultValue: ViewModelFactory = .init()
}

extension EnvironmentValues {
    var viewModelFactory: ViewModelFactory {
        get { self[ViewModelFactoryKey.self] }
        set { self[ViewModelFactoryKey.self] = newValue }
    }
}

class ViewModelFactory {
    private let setupService: SetupServiceType
    private let flightService: FlightServiceType
    private let cityService: CityServiceType

    private let setupStore: SetupStoreType
    
    init(setupService: SetupServiceType = SetupService(),
         flightService: FlightServiceType = FlightService(),
         cityService: CityServiceType = CityService(),
         setupStore: SetupStoreType = SetupStore(setupService: SetupService())) {
        self.setupStore = setupStore
        self.setupService = setupService
        self.flightService = flightService
        self.cityService = cityService
    }
    
    func makeFlightViewModel() -> FlightsViewModel {
        return FlightsViewModel(flightService: flightService,
                                setupService: setupService,
                                setupStore: setupStore)
    }
    
    func makeCitySelectionViewModel(isModal: Bool) -> CitySelectionViewModel {
        return CitySelectionViewModel(cityService: cityService,
                                      setupService: setupService,
                                      setupStore: setupStore,
                                      isModal: isModal)
    }
    
    func makePeopleSelectionViewModel(isModal: Bool) -> PeopleSelectionViewModel {
        return PeopleSelectionViewModel(setupService: setupService,
                                        setupStore: setupStore,
                                        isModal: isModal)
    }
    
    func makeFlightDetailViewModel(flightInformation: FlightInformation) -> FlightDetailViewModel {
        return FlightDetailViewModel(flightInformation: flightInformation)
    }
}

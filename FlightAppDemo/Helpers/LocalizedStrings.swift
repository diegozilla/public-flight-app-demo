//
//  LocalizedStrings.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

typealias L = AppLocalizedStrings

struct AppLocalizedStrings {
    static let generalContinue = "Continue"
    static let generalFinish = "Finish"
    static let generalSave = "Save"
}

// MARK: - Welcome Scene

extension AppLocalizedStrings {
    static let welcomeTitle = "Welcome to random flight generator!"
    static let welcomeSubtitle = "Each day we will show you up to 5 different random destinations."
    static let welcomeAction = "Start the setup"
}

// MARK: - City

extension AppLocalizedStrings {
    static let citySelectionFootnote = "If you can't find your city, dont worry, more will be available in the future."
    static let citySelectionTitle = "Select your city"
}

// MARK: - People / Passengers

extension AppLocalizedStrings {
    static let peopleTitle = "Passengers"
    static let peopleInstructions = "Enter the number of people travelling, the number must not exceed 9 between all."
    static let peopleAdults = "Adults"
    static let peopleChildren = "Children"
    static let peopleInfants = "Infants"
}

// MARK: - Flight

extension AppLocalizedStrings {
    static let flightDetailFlightTitle = "Flight:"
}

extension AppLocalizedStrings {
    static let flightDetailGeneralInformation = "General Information"
    static let flightDetailTravelRoute = "Travel Route"
    static let flightDetailMore = "More"
    static let flightDetailDepartureTime = "Departure Time"
    static let flightDetailArrivalTime = "Arrival Time"
    static let flightDetailDuration = "Duration"
    static let flightDetailPrice = "Price"
    static let flightDetailGoToWeb = "Go to the website"
}

extension AppLocalizedStrings {
    static let flightEmptyStateTitle = "We could not get the information from todays flights, please try again."
    static let flightEmptyStateTry = "Try again"
    static let flightSuggestionTitle = "Suggestions"
}

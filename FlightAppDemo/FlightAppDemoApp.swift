//
//  FlightAppDemoApp.swift
//  FlightAppDemo
//
//  Created by Diego Espinoza on 28/12/22.
//

import SwiftUI

@main
struct FlightAppDemoApp: App {
    private let factory = ViewModelFactory()
    
    var body: some Scene {
        WindowGroup {
            AppRouterView(factory: factory)
                .tint(.main)
        }
    }
}

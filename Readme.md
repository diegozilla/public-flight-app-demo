# Flight Demo App

## Assigment

Create a simple iOS app that shows 5 interesting flights to destinations using the Kiwi.com API.

The app allows the user to select the city of origin, currently the supported cities are:

- Prague
- London
- Copenhagen
- Rome
- Lisbon
- Madrid
- Berlin

Also the user will be able to select the number of passengers and will be able to change the number of: adults, children and infants.

Once this parameters are set, the app will always show 5 recommended cities to travel according to the Kiwi API for the current day.

## Search

The user will be able to change this parameters while using the app, which will trigger the results to be refreshed. To guarantee that there are always 5 results display, the app will make a request to the API changing the parameters until 5 exacts results are found. The parameters will change automatically in the following way:

- First the request will be using as parameters: current day, next day, limit as 5 and all the passenger data.
- If the request returns less than 5 cities (because the API might return less depending of the time), the app makes can increase the `departure_before` time in 12 hours, until more than 5 results are returned.
- Then the result is filtered by using the cities stored in cache from the previous search, if after the filtering the returned results are less than 5 a new search is done by increasing the limit by 10.
- Then this results will be filtered using the cached cities.
- After 5 cities are returned, this are stored in the app cache. (In this case the app is just using UserDefaults)
- Finally if the parameters are not changed/updated (same city, passengers and date), the data is returned directly from the cache to the app.

## App Architecture

- The app is using SwiftUI completely for the UI. 
- The minimum app requirement is iOS 16, because the app needs to make use of **NavigationStack**.
- The app contains 2 frameworks: `Services` and `Components`.
- The components framework has all the UI related `View`, `ViewModifiers` and other helpers.
- The services fraeworks has all the classes that connect to the API, cache and other app stored data.
- The app uses MVVM as a basic pattern to communicate the `View` with the Business Logic.
- For navigation the app uses a Router pattern which will be in charged of changing their inner view and will swap between them depending on the context.
- There are some tests to validate the model parsing is done correctly from the response and the service methods.

## Considerations
- When fetching the city images, some times the image is not displayed even tho the URL is correct. Instead of this the placeholder is displayed. Usually this is fixed whenever the app is ran again.
- As the request to the API is done until the app gets exactly the 5 cities required, the request might take some seconds to finish. But the next request will be instant because the app will used its cached data.
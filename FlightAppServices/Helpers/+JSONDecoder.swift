//
//  JSONDecoder.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

extension JSONDecoder {
    static let main: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        //decoder.dateDecodingStrategy = .iso8601
        return decoder
    }()
}

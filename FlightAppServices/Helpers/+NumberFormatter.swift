//
//  +NumberFormatter.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 30/12/22.
//

import Foundation

extension NumberFormatter {
    public static var currencyFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.groupingSeparator = " "
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        return numberFormatter
    }()
    
    public static var flightFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ""
        numberFormatter.usesGroupingSeparator = true
        return numberFormatter
    }()
}

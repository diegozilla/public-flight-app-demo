//
//  +UserDefaults.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

public enum UserDefaultKey: String {
    case hasFinishedSetup
    case setupCurrentCity
    case setupPassengers
    case cachedCities
}

extension UserDefaults {
    public static func save<T: Codable>(_ data: T, usingKey key: UserDefaultKey) {
        if let encoded = try? JSONEncoder().encode(data) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: key.rawValue)
        }
    }

    public static func fetch<T: Codable>(usingKey key: UserDefaultKey) -> T? {
        let defaults = UserDefaults.standard
        if let data = defaults.object(forKey: key.rawValue) as? Data {
            return try? JSONDecoder().decode(T.self, from: data)
        }
        return nil
    }
}

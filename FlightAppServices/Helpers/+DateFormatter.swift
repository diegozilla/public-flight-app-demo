//
//  +DateFormatter.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 30/12/22.
//

import Foundation

extension DateFormatter {
    public static var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    public static var formatterWithHours: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
        return formatter
    }()
    
    public static var flightDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d yyyy"
        return formatter
    }()
    
    public static var flightHourFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm a"
        return formatter
    }()
    
    public static var completeFlightDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy HH:mm"
        return formatter
    }()
}

extension Date {
    public var flightFormat: String {
        return DateFormatter.formatter.string(from: self)
    }
    
    public var flightFormatWithHours: String {
        return DateFormatter.formatterWithHours.string(from: self)
    }
    
    public var completeFlightFormat: String {
        return DateFormatter.completeFlightDateFormatter.string(from: self)
    }
    
    public var currentDate: Date {
        var components = Calendar.current.dateComponents([.day, .month, .year, .hour, .minute], from: self)
        components.hour = 0
        components.minute = 0
        let newDate = Calendar.current.date(from: components)
        return newDate ?? Date()
    }
    
    public var nextDay: Date {
        var components = Calendar.current.dateComponents([.day, .month, .year, .hour, .minute], from: self)
        components.day! += 1
        components.hour = 0
        components.minute = 0
        let newDate = Calendar.current.date(from: components)
        return newDate ?? Date()
    }

    public var plusTwelveHours: Date {
        currentDay(plusHours: 12)
    }

    public func currentDay(plusHours hours: Int) -> Date {
        var components = Calendar.current.dateComponents([.day, .month, .year, .hour, .minute], from: self)
        components.hour! += hours
        components.minute = 0
        let newDate = Calendar.current.date(from: components)
        return newDate ?? self
    }
}

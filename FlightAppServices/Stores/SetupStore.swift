//
//  SetupStore.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 30/12/22.
//

import Foundation
import Combine

public protocol SetupStoreType {
    var passengers: AnyPublisher<Passengers?, Never> { get }
    var city: AnyPublisher<String?, Never> { get }

    func save(currentCity city: String) async
    func save(adults: Int, children: Int, infants: Int) async
}

final public class SetupStore: SetupStoreType {
    public var passengers: AnyPublisher<Passengers?, Never> { _passengers.eraseToAnyPublisher() }
    public var city: AnyPublisher<String?, Never> { _city.eraseToAnyPublisher() }
    
    private var _passengers: CurrentValueSubject<Passengers?, Never> = .init(nil)
    private var _city: CurrentValueSubject<String?, Never> = .init(nil)

    private var setupService: SetupServiceType
    
    public init(setupService: SetupServiceType) {
        self.setupService = setupService
        setup()
    }
    
    private func setup() {
        Task {
            let passengers = try? await setupService.getPassengers()
            let city = try await setupService.getCurrentCity()
            
            self._passengers.send(passengers)
            self._city.send(city)
        }
    }
    
    public func save(currentCity city: String) async {
        await setupService.save(currentCity: city)
        _city.send(city)
    }
    
    public func save(adults: Int, children: Int, infants: Int) async {
        await setupService.save(adults: adults, children: children, infants: infants)
        _passengers.send(.init(adults: adults, children: children, infants: infants))
    }
}

//
//  Passengers.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

public struct Passengers: Codable, Equatable {
    public let adults: Int
    public let children: Int
    public let infants: Int
}

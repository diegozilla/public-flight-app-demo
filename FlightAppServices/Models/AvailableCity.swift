//
//  AvailableCity.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

public struct AvailableCity: Decodable {
    public let name: String
    public let code: String
}

//
//  FlightInformation.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

internal struct StoreFlightInformation: Codable {
    let city: String
    let date: String
    let adults: Int
    let children: Int
    let infants: Int
    let flightInformation: [FlightInformation]
}

public struct FlightInformation: Codable {
    public let id: String
    public let currency: String
    public let origin: String
    public let destination: String
    public let originCountry: String
    public let destinationCountry: String
    public let originCode: String
    public let destinationCode: String
    public let flyDuration: String
    public let price: Double
    public let deeplink: String
    public let route: [Route]
    
    public let departureTime: Date
    public let arrivalTime: Date

    public var imageURL: URL? { URL(string: "https://images.kiwi.com/photos/600x330/\(cityCode).jpg") }
    public var deeplinkURL: URL? { URL(string: deeplink) }
    
    private let cityCode: String
    
    init(response: FlightInformationResponse, currency: String) {
        self.currency = currency
        self.id = response.id
        self.origin = response.cityFrom
        self.destination = response.cityTo
        self.originCountry = response.countryFrom.name
        self.destinationCountry = response.countryTo.name
        self.originCode = response.cityCodeFrom
        self.destinationCode = response.cityCodeTo
        self.flyDuration = response.flyDuration
        self.price = response.price
        self.cityCode = "\(response.cityTo.lowercased())_\(response.countryTo.code.lowercased())"
        self.departureTime = Date(timeIntervalSince1970: response.dTimeUTC)
        self.arrivalTime = Date(timeIntervalSince1970: response.aTimeUTC)
        self.deeplink = response.deepLink
        self.route = response.route.map(FlightInformation.Route.init)
    }
    
    public static let stub = FlightInformation(response: .stub, currency: "EU")
    public static let badStub = FlightInformation(response: .badStub, currency: "EU")
}

extension FlightInformation {
    public struct Route: Codable {
        public let id: String
        public let cityFrom: String
        public let cityTo: String

        public let departureDate: Date
        public let arrivalDate: Date

        public let airline: String
        public let flightNumber: Int

        init(route: FlightInformationResponse.Route) {
            self.id = route.id
            self.cityTo = route.cityTo
            self.cityFrom = route.cityFrom
            self.departureDate = Date(timeIntervalSince1970: route.dTimeUTC)
            self.arrivalDate = Date(timeIntervalSince1970: route.aTimeUTC)
            self.airline = route.airline
            self.flightNumber = route.flightNo
        }
    }
    
}

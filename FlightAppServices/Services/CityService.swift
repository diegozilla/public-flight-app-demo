//
//  CityService.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

public protocol CityServiceType {
    func fetchAvailableCities() async throws -> [AvailableCity]
}

private class CityBundleHelper { }

public actor CityService: CityServiceType {
    public init() { }

    public func fetchAvailableCities() async throws -> [AvailableCity] {
        let bundle = Bundle(for: CityBundleHelper.self)
        guard let url = bundle.url(forResource: "AvailableCities", withExtension: "json") else {
            throw CityError.cannotLoad
        }

        do {
            let data = try Data(contentsOf: url)
            let jsonData = try JSONDecoder.main.decode([AvailableCity].self, from: data)
            return jsonData
        } catch {
            throw CityError.cannotLoad
        }
    }
}

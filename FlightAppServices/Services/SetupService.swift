//
//  SetupService.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

public protocol SetupServiceType {
    func save(currentCity city: String) async
    func save(adults: Int, children: Int, infants: Int) async
    
    func getCurrentCity() async throws -> String
    func getPassengers() async throws -> Passengers
}

public actor SetupService: SetupServiceType {
    public init() { }
    
    public func save(currentCity city: String) async {
        UserDefaults.save(city, usingKey: .setupCurrentCity)
    }

    public func save(adults: Int, children: Int, infants: Int) async {
        let passengers = Passengers(adults: adults,
                                   children: children,
                                   infants: infants)
        UserDefaults.save(passengers, usingKey: .setupPassengers)
    }

    public func getCurrentCity() async throws -> String {
        let city: String? = UserDefaults.fetch(usingKey: .setupCurrentCity)
        guard let city = city else { throw SetupError.noCity }
        return city
    }

    public func getPassengers() async throws -> Passengers {
        let passengers: Passengers? = UserDefaults.fetch(usingKey: .setupPassengers)
        guard let passengers = passengers else { throw SetupError.noPassengers }
        return passengers
    }
}

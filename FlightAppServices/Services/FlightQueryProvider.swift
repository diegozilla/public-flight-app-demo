//
//  FlightQueryProvider.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 2/01/23.
//

import Foundation

public protocol FlightQueryProviderType {
    func getFlights(city: String,
                    adults: Int,
                    children: Int,
                    infants: Int,
                    departAfter: String,
                    departBefore: String,
                    limit: Int) async throws -> [FlightInformation]
}

public actor FlightQueryProvider: FlightQueryProviderType {
    public init() {}

    public func getFlights(city: String,
                           adults: Int,
                           children: Int,
                           infants: Int,
                           departAfter: String,
                           departBefore: String,
                           limit: Int) async throws -> [FlightInformation] {

        guard let url = URL(string: "https://api.skypicker.com/flights?v3&partner=skypicker&locale=en&fly_from=\(city)&fly_to=anywhere&limit=\(limit)&depart_after=\(departAfter)&depart_before=\(departBefore)&adults=\(adults)&children=\(children)&infants=\(infants)&typeFlight=oneway&one_per_date=0&one_for_city=1&sort=popularity&asc=0&wait_for_refresh=0") else { throw FlightError.urlBuilding }
        let urlSession = URLSession(configuration: .default)
        
        let (data, _) = try await urlSession.data(for: URLRequest(url: url))
        let decoded = try JSONDecoder.main.decode(ResponseContainer<FlightInformationResponse>.self, from: data)
        let currency = decoded.currency
        
        let flightInformation = decoded.data.map { response in
            FlightInformation(response: response, currency: currency)
        }
        
        return flightInformation
    }
}

//
//  FlightService.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

public protocol FlightServiceType {
    func fetchAndCacheRecommendedFlights(from city: String,
                                         adults: Int,
                                         children: Int,
                                         infants: Int,
                                         from: Date,
                                         to: Date) async throws -> [FlightInformation]
    
    func fetchRecommendedFlights(from city: String,
                                 adults: Int,
                                 children: Int,
                                 infants: Int,
                                 from: Date,
                                 to: Date,
                                 limit: Int) async throws -> [FlightInformation]
}

public actor FlightService: FlightServiceType {

    private static let baseQueryLimit = 5
    private let provider: FlightQueryProviderType
    
    public init(provider: FlightQueryProviderType = FlightQueryProvider()) {
        self.provider = provider
    }
    
    /**
        Use this method for getting always 5 recommended flights for the user based on his location and number of passengers. This method will cache the results for the day and if no parameter was changed (city, passengers) will always returned the stored data until the next day.
     
        As the requirement is that there are always 5 recommended flights and that no city is shown again in case it was presented the day before, the logic is the following:
     
        1. Check if there is already a stored cached flight.
        2. Compare if the parameters have changed (so a new search is needed) or if the day changed (new results are needed)
        3. In case there is no need of a new query, the cached flights are returned
        4. In case a parameter changed, a new query is prepared.
        5. To be 100% sure that always 5 results are presented, we change some parameters of the query to be sure we will always get the 5 results.
            5a. Depending on the time the query is done, the API might return no results (because of departure times), so to prevent getting no data, the query will increase the `departureBefore` time in 12 hours so that more results are found.
            5b. As the cities presented cannot be the same as the ones from the previous day, there is a comparisson done with the stored flight information. If the result of the filtering is less than 5, the query limit is increased by 10 so that more results are returned.
        6. Once the query gets 5 not repeating results, the new information is stored and the result is returned to be displayed.
     
     */
    
    public func fetchAndCacheRecommendedFlights(from city: String,
                                                adults: Int,
                                                children: Int,
                                                infants: Int,
                                                from: Date,
                                                to: Date) async throws -> [FlightInformation] {
        let storedFlight = getCachedFlights()
        
        if let stored = storedFlight,
           stored.city == city && stored.date == from.flightFormat
            && stored.adults == adults && stored.children == children && stored.infants == infants {
            return stored.flightInformation
        } else {
            var flights: [FlightInformation] = []
            var flightLimit = Self.baseQueryLimit
            let storedDestinationCodes: [String]
            
            if storedFlight?.city == city
                && storedFlight?.adults == adults && storedFlight?.children == children && storedFlight?.infants == infants {
                storedDestinationCodes = storedFlight?.flightInformation.map(\.destinationCode) ?? []
            } else {
                storedDestinationCodes = []
            }

            while flights.count < Self.baseQueryLimit {
                print("1. fetching with: limit \(flightLimit) ")
                let results = try await fetchRecommendedFlights(from: city,
                                                                adults: adults,
                                                                children: children,
                                                                infants: infants,
                                                                from: from,
                                                                to: to,
                                                                limit: flightLimit)
                flights = results.filter { flight in
                    !storedDestinationCodes.contains(where: { $0 == flight.destinationCode })
                }
                flightLimit += 10
            }
            let finalFlights = Array(flights.prefix(Self.baseQueryLimit))
            print("1. finish fetching with: limit \(flightLimit - 10) total: \(finalFlights.count) ")
            saveFlights(forDate: from,
                        city: city,
                        adults: adults,
                        children: children,
                        infants: infants,
                        flights: finalFlights)
            return finalFlights
        }
    }
    
    public func fetchRecommendedFlights(from city: String,
                                        adults: Int,
                                        children: Int,
                                        infants: Int,
                                        from: Date,
                                        to: Date,
                                        limit: Int) async throws -> [FlightInformation] {
        let departAfter = from.currentDate
        var departBefore = to.currentDate
        var flights: [FlightInformation] = []
        
        do {
            while flights.count < Self.baseQueryLimit {
                print("2. fetching with: limit \(limit) and date \(departBefore)")
                flights = try await provider.getFlights(city: city,
                                                        adults: adults,
                                                        children: children,
                                                        infants: infants,
                                                        departAfter: departAfter.flightFormatWithHours,
                                                        departBefore: departBefore.flightFormatWithHours,
                                                        limit: limit)
                departBefore = departBefore.plusTwelveHours
            }
            print("2. finish fetching with: limit \(limit) and date \(departBefore) minus 12")
            return flights
        } catch {
            throw FlightError.cannotFetchData
        }
    }

    // MARK: - Helper methods

    private func getCachedFlights() -> StoreFlightInformation? {
        let stored: StoreFlightInformation? = UserDefaults.fetch(usingKey: .cachedCities)
        guard let stored = stored else { return nil }
        return stored
    }
    
    private func saveFlights(forDate date: Date,
                             city: String,
                             adults: Int,
                             children: Int,
                             infants: Int,
                             flights: [FlightInformation]) {
        let stored = StoreFlightInformation(city: city,
                                            date: date.flightFormat,
                                            adults: adults,
                                            children: children,
                                            infants: infants,
                                            flightInformation: flights)
        UserDefaults.save(stored, usingKey: .cachedCities)
    }
}

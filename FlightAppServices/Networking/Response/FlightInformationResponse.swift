//
//  FlightInformationResponse.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

public struct FlightInformationResponse: Decodable {
    public let id: String
    
    public let flyFrom: String
    public let flyTo: String
    public let cityFrom: String
    public let cityTo: String
    
    public let cityCodeFrom: String
    public let cityCodeTo: String
    
    public let countryFrom: Country
    public let countryTo: Country
    public let distance: Double
    public let duration: Duration
    
    public let flyDuration: String
    public let price: Double
    
    public let aTimeUTC: Double
    public let dTimeUTC: Double

    public let route: [Route]
    public let deepLink: String
}

extension FlightInformationResponse {
    public struct Country: Decodable {
        public let code: String
        public let name: String
    }
    
    public struct Duration: Decodable {
        public let departure: Int
        public let `return`: Int
        public let total: Int
    }
    
    public struct Route: Decodable {
        let id: String
        let cityFrom: String
        let cityTo: String
        
        let dTimeUTC: TimeInterval
        let aTimeUTC: TimeInterval
        
        let airline: String
        let flightNo: Int
        let operatingCarrier: String
        let operatingFlightNo: String
    }
}

extension FlightInformationResponse {
    static let stub = FlightInformationResponse(id: "123",
                                                flyFrom: "PRG",
                                                flyTo: "LON",
                                                cityFrom: "Prague",
                                                cityTo: "London",
                                                cityCodeFrom: "PRG",
                                                cityCodeTo: "LON",
                                                countryFrom: .init(code: "cz", name: "Czech Republic"),
                                                countryTo: .init(code: "gb", name: "Great Britain"),
                                                distance: 123,
                                                duration: .init(departure: 1000, return: 0, total: 1000),
                                                flyDuration: "2h 50m",
                                                price: 234,
                                                aTimeUTC: 1672398000,
                                                dTimeUTC: 1672398000,
                                                route: [.init(id: "1234",
                                                              cityFrom: "Prague",
                                                              cityTo: "London",
                                                              dTimeUTC: 1672398000,
                                                              aTimeUTC: 1672408200,
                                                              airline: "PC",
                                                              flightNo: 920,
                                                              operatingCarrier: "PC",
                                                              operatingFlightNo: "921"
                                                             ),
                                                        .init(id: "1235",
                                                              cityFrom: "London",
                                                              cityTo: "Berlin",
                                                              dTimeUTC: 1672398000,
                                                              aTimeUTC: 1672408200,
                                                              airline: "PC",
                                                              flightNo: 920,
                                                              operatingCarrier: "PC",
                                                              operatingFlightNo: "921"
                                                             ),
                                                ], deepLink: "https://www.kiwi.com/deep?affilid=skypicker&currency=EUR&flightsId=0042043c4b9c00004ab4a0ba_0%7C0042043c4b9c00004ab4a0ba_1%7C043c21dd4b9d00003f667336_0&from=BER&lang=en&passengers=1&to=DXB&booking_token=Eb1GSvp-pJJLJKdcgIzoF6DXkT759kReSBZY4E48JgQkcqF0WzVHKz7Kke3uzWJh02vd0t8JsApF70k_aWx--aCY48W0EcukPSntE-AXMEG_9ngU41OmXylgTmKo8DMzLwTxrTXFEYVbJFkGCtN_FoYF7Yi09ZJ7wrvji49kyHIWtpbIvBv-U3BIzL6ydiqjTRJlvc0CED6KB3VAbpP4zIqyMkyEUDGR-ZlwlSPTAScEH4Q-MXPEXTYZJfRaLBqGoVF019BN-mJbLk7Hp19O-yqjdG5gGArlclnHJ2MGRjOOLaVPJJUpxCd8WlymfRR4TOqAqc1Z877XGbuLprKv2JD8DTHj3VYkJrNu0Px22d3NA43PVBGhmaJ0Hflh_-ssRZOSDh8OJhIb-st9e2GNdhz1-_IcNCVYHtWXhQL8JnB0zphFnHLrITm17XmmYXziFtdALpr1xAaNAlKW8ZyTKW34fxXrtVHijLeGcBPzRfs_7tDek9U9EDBleXb8omBFLMz9Qn4-v8CR3mlrcdxUCrTDKBP-sUTUL7_5-0Ah25tc=")
    
    static let badStub = FlightInformationResponse(id: "123",
                                                   flyFrom: "PRG",
                                                   flyTo: "LON",
                                                   cityFrom: "Prague",
                                                   cityTo: "London",
                                                   cityCodeFrom: "PRG",
                                                   cityCodeTo: "LON",
                                                   countryFrom: .init(code: "cz", name: "Czech Republic"),
                                                   countryTo: .init(code: "gbad", name: "Great Britain"),
                                                   distance: 123,
                                                   duration: .init(departure: 1000, return: 0, total: 1000),
                                                   flyDuration: "2h 50m",
                                                   price: 234,
                                                   aTimeUTC: 1672398000,
                                                   dTimeUTC: 1672398000,
                                                   route: [],
                                                   deepLink: "")
}

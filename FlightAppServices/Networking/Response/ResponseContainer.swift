//
//  ResponseContainer.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

struct ResponseContainer<T: Decodable>: Decodable {
    let searchId: String
    let currency: String
    let fxRate: Int
    let data: [T]
}

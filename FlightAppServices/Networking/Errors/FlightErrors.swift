//
//  FlightErrors.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

public enum FlightError: Error {
    case cannotFetchData
    case incorrectData
    case urlBuilding
}

extension FlightError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .cannotFetchData, .incorrectData, .urlBuilding:
            return "We can't get the information at the moment, please try again later."
        }
    }
}

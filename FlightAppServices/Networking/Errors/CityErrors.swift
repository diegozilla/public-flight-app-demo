//
//  CityErrors.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 29/12/22.
//

import Foundation

enum CityError: Error {
    case cannotLoad
}

extension CityError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .cannotLoad:
            return "The available cities could not be loaded"
        }
    }
}

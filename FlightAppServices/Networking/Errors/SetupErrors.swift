//
//  SetupError.swift
//  FlightAppServices
//
//  Created by Diego Espinoza on 30/12/22.
//

import Foundation

public enum SetupError: Error {
    case noCity
    case noPassengers
}

extension SetupError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .noCity:
            return "Could not get the user city"
        case .noPassengers:
            return "Could not get the number of travelling passengers"
        }
    }
}

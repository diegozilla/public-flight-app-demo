//
//  StateContainerView.swift
//  FlightAppComponents
//
//  Created by Diego Espinoza on 29/12/22.
//

import SwiftUI

public struct StateContainerView<Content: View>: View {
    let error: String?
    let isLoading: Bool
    let content: Content
    
    @State var test: String? = nil
    private let transition: AnyTransition = .move(edge: .top).combined(with: .opacity)
    
    public init(error: String?,
                isLoading: Bool,
                content: Content) {
        self.error = error
        self.isLoading = isLoading
        self.content = content
    }

    public var body: some View {
        ZStack {
            if let error = error {
                ServerErrorBannerView(errorMessage: error)
                    .frame(maxHeight: .infinity, alignment: .top)
                    .transition(transition)
                    .zIndex(1)
            }
            
            if isLoading {
                ProgressView()
                    .frame(width: 100, height: 100)
                    .background(Material.ultraThinMaterial,
                                in: RoundedRectangle(cornerSize: .medium))
                    .transition(.opacity.combined(with: .scale))
                    .background(Color.label.opacity(0.1))
                    .cornerRadius(.medium)
                    .tint(.secondaryLabel)
                    .zIndex(2)
            }

            content
                .zIndex(0)
        }
        .animation(.easeInOut(duration: 0.25), value: isLoading)
        .animation(.interactiveSpring(response: 0.5, dampingFraction: 0.9), value: error)
    }
}

private struct ServerErrorBannerView: View {
    let errorMessage: String

    var body: some View {
        HStack {
            Image(systemName: "x.circle")
            
            Text(errorMessage)
        }
        .font(.subheadline)
        .padding(.vertical, .medium)
        .padding(.horizontal, .xLarge)
        .frame(maxWidth: .infinity, alignment: .leading)
        .background(
            RoundedRectangle(cornerRadius: .small)
                .fill(Color.danger)
                .padding(.horizontal, .medium)
        )
        .padding(.top, .xSmall)
        .foregroundColor(.label)
    }
}

struct StateContainerView_Previews: PreviewProvider {
    static var text: some View {
        Text("This is a test")
    }

    static var previews: some View {
        StateContainerView(error: nil,
                           isLoading: true,
                           content: text)
        
        StateContainerView(error: "This is an error",
                           isLoading: false,
                           content: text)
    }
}

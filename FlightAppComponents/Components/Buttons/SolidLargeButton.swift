//
//  SolidLargeButton.swift
//  FlightAppComponents
//
//  Created by Diego Espinoza on 28/12/22.
//

import SwiftUI

public struct SolidButton: View {
    let title: String
    let useMaxWidth: Bool
    let onAction: () -> Void
    
    public init(title: String,
                useMaxWidth: Bool = true,
                onAction: @escaping () -> Void) {
        self.title = title
        self.useMaxWidth = useMaxWidth
        self.onAction = onAction
    }
    
    public var body: some View {
        Button {
            onAction()
        } label: {
            Text(title)
                .fontWeight(.semibold)
                .frame(maxWidth: useMaxWidth ? .infinity : nil)
        }
        .controlSize(.large)
        .buttonStyle(.borderedProminent)
    }
}

struct SolidButton_Previews: PreviewProvider {
    static var previews: some View {
        VStack(spacing: .large) {
            SolidButton(title: "Continue",
                        onAction: {})
            
            SolidButton(title: "Continue",
                        onAction: {})
                .disabled(true)
            
            SolidButton(title: "Continue",
                        useMaxWidth: false,
                        onAction: {})
            
            
            SolidButton(title: "Continue",
                        useMaxWidth: false,
                        onAction: {})
            .disabled(true)
        }
        .padding()
    }
}

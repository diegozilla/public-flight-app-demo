//
//  TextStyle.swift
//  FlightAppComponents
//
//  Created by Diego Espinoza on 2/01/23.
//

import SwiftUI

struct LargeTitleStyle: ViewModifier {
    func body(content: Content) -> some View {
        content.foregroundColor(.label)
            .font(.largeTitle)
            .fontWeight(.bold)
    }
}

struct SecondaryBoldTitleStyle: ViewModifier {
    func body(content: Content) -> some View {
        content.font(.title3)
            .fontWeight(.bold)
    }
}

struct SecondaryTitleStyle: ViewModifier {
    func body(content: Content) -> some View {
        content.font(.title3)
            .fontWeight(.medium)
    }
}

struct SectionTitleStyle: ViewModifier {
    func body(content: Content) -> some View {
        content.foregroundColor(.label)
            .font(.headline)
            .fontWeight(.semibold)
    }
}

struct SubtitleStyle: ViewModifier {
    func body(content: Content) -> some View {
        content.font(.headline)
            .fontWeight(.regular)
    }
}

struct InformationTextStyle: ViewModifier {
    func body(content: Content) -> some View {
        content.font(.headline)
            .fontWeight(.medium)
    }
}

struct DetailTextStyle: ViewModifier {
    func body(content: Content) -> some View {
        content.font(.subheadline)
            .fontWeight(.regular)
    }
}

struct SmallTitleStyle: ViewModifier {
    func body(content: Content) -> some View {
        content.font(.subheadline)
            .fontWeight(.semibold)
    }
}

extension View {
    public func largeTitleStyle() -> some View {
        modifier(LargeTitleStyle())
    }
    
    public func secondaryBoldTitleStyle() -> some View {
        modifier(SecondaryBoldTitleStyle())
    }

    public func secondaryTitleStyle() -> some View {
        modifier(SecondaryTitleStyle())
    }
    
    public func sectionTitleStyle() -> some View {
        modifier(SectionTitleStyle())
    }
    
    public func subtitleStyle() -> some View {
        modifier(SubtitleStyle())
    }
    
    public func informationTextStyle() -> some View {
        modifier(InformationTextStyle())
    }
    
    public func detailTextStyle() -> some View {
        modifier(DetailTextStyle())
    }
    
    public func smallTextStyle() -> some View {
        modifier(SmallTitleStyle())
    }
}

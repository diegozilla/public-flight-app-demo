//
//  Sizes.swift
//  FlightAppComponents
//
//  Created by Diego Espinoza on 28/12/22.
//

import Foundation

extension CGFloat {
    /// value: 4
    public static let tiny: CGFloat = 4
    /// value: 8
    public static let small: CGFloat = 8
    /// value: 12
    public static let xSmall: CGFloat = 12
    /// value: 16
    public static let medium: CGFloat = 16
    /// value: 20
    public static let xMedium: CGFloat = 20
    /// value: 24
    public static let large: CGFloat = 24
    /// value: 32
    public static let xLarge: CGFloat = 32
}

extension CGSize {
    /// value: 4
    public static let tiny = CGSize(width: CGFloat.tiny, height: CGFloat.tiny)
    /// value: 8
    public static let small = CGSize(width: CGFloat.small, height: CGFloat.small)
    /// value: 12
    public static let xSmall = CGSize(width: CGFloat.xSmall, height: CGFloat.xSmall)
    /// value: 16
    public static let medium = CGSize(width: CGFloat.medium, height: CGFloat.medium)
    /// value: 20
    public static let xMedium = CGSize(width: CGFloat.xMedium, height: CGFloat.xMedium)
    /// value: 24
    public static let large = CGSize(width: CGFloat.large, height: CGFloat.large)
    /// value: 32
    public static let xLarge = CGSize(width: CGFloat.xLarge, height: CGFloat.large)
}

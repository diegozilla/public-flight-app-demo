//
//  Colors.swift
//  FlightAppComponents
//
//  Created by Diego Espinoza on 28/12/22.
//

import SwiftUI

extension Color {
    public static let label = Color(uiColor: .label)

    public static let secondaryLabel = Color(uiColor: .secondaryLabel)
    
    public static let tertiaryLabel = Color(uiColor: .tertiaryLabel)
    
    public static let groupedBackground = Color(uiColor: .systemGroupedBackground)
    
    public static let secondaryGroupedBackground = Color(uiColor: .secondarySystemGroupedBackground)
    
    public static let background = Color(uiColor: .systemBackground)
    
    public static let secondaryBackground = Color(uiColor: .secondarySystemBackground)
    
    public static let main = Color(uiColor: .systemBlue)
    
    public static let danger = Color(uiColor: .systemRed)
    
    public static let warning = Color(uiColor: .systemOrange)
}
